from django.shortcuts import render, redirect
from django.conf import settings
from django.core.files.storage import FileSystemStorage

# Create your views here.
#views.py
from login.forms import *
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
from django.template import RequestContext

from login.models import Document
from login.forms import DocumentForm

import networkx as nx
import simplejson as json
import os.path
from networkx.readwrite import json_graph

def register_success(request):
    return render_to_response(
    'registration/success.html',
    )
 
def logout_page(request):
    logout(request)
    return HttpResponseRedirect('/')
 
@login_required
def home(request):
    documents = Document.objects.all()
    return render(request, 'home.html', { 'documents': documents })
    #return render_to_response(
    #'home.html',
    #{ 'user': request.user }
    #)

def model_form_upload(request):
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            gml_to_json(settings.MEDIA_ROOT + "\documents\\" + str(request.FILES['document']))
            return HttpResponseRedirect('/home')    
    else:
        form = DocumentForm()
    return render(request, 'model_form_upload.html', {
        'form': form
    })

def delete_file(request):
    documents = Document.objects.all()
    if documents:
        for document in documents:
            document.delete()
    return render(request, 'home.html', { 'documents': documents })

def gml_to_json(filename):
    gml = open(filename)
    g = nx.read_gml(gml)
    d = json_graph.node_link_data(g)
    json_file = filename.replace('documents/', '').replace('.gml', '.json')
    with open(json_file, 'w') as outfile:
        json.dump(d, outfile)
    return HttpResponseRedirect('/home')

def run_python_script(request):
    node = request.GET['node']
    amount = request.GET['amount']
    os.system('python ' + settings.MEDIA_ROOT + '\send-money.py ' + amount + ' ' + node + ' 192.168.10.100')
    return HttpResponseRedirect('/home')

