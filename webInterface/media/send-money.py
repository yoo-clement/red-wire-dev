import sys, getopt, requests
import simplejson as json

def main(argv):
	url = "http://127.0.0.1:5000/create-transaction"
	headers = {'Content-type': 'application/json'}
	try:
	  opts, args = getopt.getopt(argv,"ha:")
	except getopt.GetoptError:
	  print('Usage : python send_money.py <sender> <amount>')
	  sys.exit(2)
	for opt, arg in opts:
	  if opt == '-h':
	     print('Usage : python.py send_money.py <amount> <sender> <recipient>')
	     sys.exit()
	data = {'sender': sys.argv[2], 'recipient': sys.argv[3], 'amount': sys.argv[1]}
	create_transaction = requests.post(url, data=json.dumps(data), headers=headers)
	mine_block = requests.get('http://127.0.0.1:5000/mine')
	print(sys.argv[1], 'euros de plus dans la cagnotte. Merci pour votre participation.')

if __name__ == "__main__":
   main(sys.argv[1:])
